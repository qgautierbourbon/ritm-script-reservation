import json

import requests

from utils import print_response


def get_remaining_places(task_id, center_id, token):
    url = f"https://app.heitzfit.com/c/{center_id}/ws/api/planning/scheduledTask/{task_id}?stats=true&task=true&room=true&group=true&coach=true&teamConfig=true"
    headers = {
        'Content-Type': 'application/json',
        'Authorization': token,
        'Cache-Control': 'no-cache',
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        print_response("check remaining place", response.json())
        data = json.loads(response.content)
        max_places = data['placesMax']
        active_participants = data['_stats']['active']
        remaining_places = max_places - active_participants
        return remaining_places
    else:
        print(
            f"Erreur lors de la récupération des informations sur la tâche {task_id}: statut HTTP {response.status_code}")
        return None


def count_remaining_passages(subscriptions):
    remaining_passages = sum(
        [subscription['totalPassages'] - subscription['lastPassage'] for subscription in subscriptions])
    return remaining_passages
