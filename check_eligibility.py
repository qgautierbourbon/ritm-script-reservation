import json

import requests

from heitzfit_reservation import token, center_id
from remaining_places import count_remaining_passages
from utils import print_response


def check_eligibility(task_id, client_id, places, target_marketplace):
    eligibility_url = 'https://app.heitzfit.com/c/' + str(center_id) + '/ws/api/planning/scheduledTask/' + str(
        task_id) + '/checkEligibility'
    eligibility_payload = {
        "idClient": client_id,
        "places": places,
        "targetMarketPlace": target_marketplace
    }

    headers = {
        'Content-Type': 'application/json',
        'Authorization': token,
        'Cache-Control': 'no-cache'
    }

    response = requests.post(eligibility_url, data=json.dumps(eligibility_payload), headers=headers)
    print_response("Vérification de l'éligibilité du cours", response.json())

    if response.status_code == 200:
        task_details = response.json()
        user_subscriptions = task_details.get('userSubscriptions', [])

        remaining_passages = count_remaining_passages(user_subscriptions)

        if remaining_passages >= places:
            task_details['bookingUrl'] = task_details['task'].get('bookingUrl')
            return True, task_details
        else:
            print("L'utilisateur n'a pas assez de passages restants pour participer à ce cours.")
            return False, None
    else:
        print("Vérification de l'éligibilité a échoué. Statut HTTP : ", response.status_code)
        print("Message d'erreur : ", response.json().get('message'))
        return False, None
