# Fonction pour récupérer les données depuis l'API
from utils import call_api


def fetch_data_from_api(start_date, num_days=7):
    center_id = 4647
    api_endpoint = f"https://app.heitzfit.com/c/{center_id}/ws/api/planning/browse"
    params = {
        "startDate": start_date,
        "numberOfDays": num_days,
        "idActivities": "",
        "idEmployees": "",
        "idRooms": "",
        "idGroups": "",
        "hourStart": "",
        "hourEnd": "",
        "stackBy": "date",
        "caloriesMin": "",
        "caloriesMax": ""
    }

    raw_data = call_api(api_endpoint, params)

    if raw_data is None:
        print("Les données JSON de l'API ne peuvent pas être chargées.")
        return None

    return raw_data
