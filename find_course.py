import datetime

from fetch_courses import fetch_data_from_api
from utils import print_response, parse_date


class NoMatchingCourseFoundException(Exception):
    def __init__(self, msg="Aucun cours correspondant trouvé"):
        super().__init__(msg)


def find_matching_course(activity_label, target_date, hour_start):
    target_dt = datetime.strptime("{} {}".format(target_date, hour_start), "%Y-%m-%d %H:%M")

    schedule_by_dates = fetch_data_from_api(target_date)

    for date_key, activities in schedule_by_dates.items():
        if date_key == target_date:
            for activity in activities:
                print_response("activités", activity)
                if activity_label.lower().strip() in activity["activity"].lower().strip():
                    if target_dt >= parse_date(date_key + " " + activity["start"][11:]):
                        return activity["id"], date_key

    raise NoMatchingCourseFoundException()
