import datetime
import json
import pprint

import requests


def parse_date(date_string):
    dt = datetime.strptime(date_string[:10], "%Y-%m-%d").replace(tzinfo=None)
    return dt


def get_current_date():
    """Retourne la date actuelle au format année-mois-jour."""
    return datetime.now().strftime("%Y-%m-%d")


def print_response(title, response):
    print(f"\n=== {title} ===")
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(response)


def display_json(title, json_dict):
    print("\n=== {} ===".format(title))
    print(json.dumps(json_dict, indent=4))

    # Fonction helper pour appeler l'API


def call_api(url, params=None):
    headers = {
        "Accept": "*/*",
        "User-Agent": "Thunder Client (https://www.thunderclient.com)"
    }

    try:
        response = requests.get(url, params=params, headers=headers)
        response.raise_for_status()
    except requests.HTTPError as http_error:
        print(f"HTTP error occurred: {http_error}")
        return None
    except Exception as other_error:
        print(f"Other error occurred: {other_error}")
        return None

    if response.ok:
        return response.json()

    return None
