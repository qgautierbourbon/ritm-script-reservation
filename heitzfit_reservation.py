import json

import requests

from find_course import find_matching_course
from propriete import email, password, app_id, center_id
from reserve_spot import reserve_spot
from utils import print_response

# Connexion à l'API
login_url = 'https://app.heitzfit.com/c/' + str(center_id) + '/ws/api/auth/signin'
login_payload = {
    "email": email,
    "password": password,
    "_appId": app_id
}
response = requests.post(login_url, data=json.dumps(login_payload), headers={'Content-Type': 'application/json'})
result = response.json()
print_response("Connexion à l'API", response.json())
if result and 'token' in result:
    token = result['token']
    headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}
else:
    print("Échec de la connexion à l'API. Erreur : ", result.get('message'))
    exit()

# Prompt the user for inputs
activity_label = input("Enter the desired course name: ")
target_date = input("Enter the target date (yyyy-mm-dd): ")
hour_start = input("Enter the starting time (hh:mm): ")

# Call the function with these inputs
course_id, course_date = find_matching_course(activity_label, target_date, hour_start)
print_response(
    "ID du cours trouvé pour le {}, date {} à {} : {}".format(activity_label, target_date, hour_start, course_id), '')

reserve_spot(course_id, center_id, token)
