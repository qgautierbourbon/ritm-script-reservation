import json

import requests

from remaining_places import get_remaining_places
from utils import print_response


def reserve_spot(task_id, center_id, token):
    # Get remaining places
    remaining_places = get_remaining_places(task_id, center_id, token)

    if remaining_places is None or remaining_places <= 0:
        print("Aucune place disponible pour la réservation.")
        return

    booking_payload = {
        "id": task_id,
        "joinQueueList": False,
        "places": 1
    }

    booking_url = f"https://app.heitzfit.com/c/{center_id}/ws/api/planning/book"
    headers = {
        'Content-Type': 'application/json',
        'Authorization': token,
        'Cache-Control': 'no-cache',
    }

    response = requests.post(booking_url, data=json.dumps(booking_payload), headers=headers)

    print_response("response book ", response.json())
    if response.status_code != 200:
        print("Échec de la réservation. Statut HTTP :", response.status_code)
    else:
        print("Réservation réussie!")
